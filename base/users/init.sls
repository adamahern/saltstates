user_adam_ahern:
  user.present:
    - name: adam.ahern
    - fullname: Adam Ahern
    - shell: /bin/bash
    - home: /home/adam.ahern
    - uid: 10000
    - groups:
      - wheel

adam_key:
  ssh_auth.present:
    - name: adam.ahern
    - user: adam.ahern
    - source: salt://users/keys/adam.ahern.pub