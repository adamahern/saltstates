user_adam_dev:
  user.present:
    - name: adam.dev
    - fullname: Adam Ahern
    - shell: /bin/bash
    - home: /home/adam.dev
    - uid: 10001
    - groups:
      - wheel

adam_dev_key:
  ssh_auth.present:
    - name: adam.dev
    - user: adam.dev
    - source: salt://users/keys/adam.dev.pub